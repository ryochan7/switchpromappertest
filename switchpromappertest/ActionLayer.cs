﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace switchpromappertest
{
    public class ActionLayer
    {

        /// <summary>
        /// Contains MapAction instances only associated with this ActionLayer
        /// </summary>
        private List<MapAction> layerActions = new List<MapAction>(20);
        public List<MapAction> LayerActions { get => layerActions; }

        private string name;

        public string Name { get => name; set => name = value; }

        private string description;
        public string Description { get => description; set => description = value; }

        public ActionLayer()
        {
        }
    }
}
