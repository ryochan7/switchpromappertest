﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using Nefarius.ViGEm.Client;
using Nefarius.ViGEm.Client.Targets;
using Nefarius.ViGEm.Client.Targets.Xbox360;
using Sensorit.Base;
using switchpromappertest.SwitchProLibrary;

namespace switchpromappertest
{
    public class Mapper
    {
        private const ushort STICK_MAX = 3200;
        private const ushort STICK_MIN = 500;

        private const int inputResolution = STICK_MAX - STICK_MIN;
        private const float reciprocalInputResolution = 1 / (float)inputResolution;

        private const int X360_STICK_MAX = 32767;
        private const int X360_STICK_MIN = -32768;
        private const int OUTPUT_X360_RESOLUTION = X360_STICK_MAX - X360_STICK_MIN;

        private const ushort STICK_MIDRANGE = inputResolution / 2;
        private const ushort STICK_NEUTRAL = STICK_MAX - STICK_MIDRANGE;

        private const int MOUSESPEEDFACTOR = 20;
        private const double MOUSESTICKOFFSET = 0.0495;
        private const int MOUSESPEED = 100;
        private const double MOUSE_VELOCITY_OFFSET = 0.06;

        private int mouseSpeed = MOUSESPEED;
        //private int mouseXSpeed = MOUSESPEED;
        //private int mouseYSpeed = MOUSESPEED;
        private double mouseX = 0.0;
        private double mouseY = 0.0;
        private double mouseXRemainder = 0.0;
        private double mouseYRemainder = 0.0;
        //private OneEuroFilter filterX = new OneEuroFilter(minCutoff: 2.0, beta: 0.2);
        //private OneEuroFilter filterY = new OneEuroFilter(minCutoff: 2.0, beta: 0.2);
        private OneEuroFilter filterX = new OneEuroFilter(minCutoff: 0.4, beta: 0.2);
        private OneEuroFilter filterY = new OneEuroFilter(minCutoff: 0.4, beta: 0.2);
        private double currentRate = 1.0; // Expressed in Hz
        private double currentLatency = 1.0; // Expressed in sec

        private bool mouseLBDown;
        private bool mouseRBDown;

        private Thread vbusThr;
        private Thread contThr;

        private ViGEmClient vigemTestClient = null;
        private IXbox360Controller outputX360 = null;
        private SwitchProEnumerator proDeviceEnumerator;
        private Dictionary<SwitchProDevice, SwitchProReader> deviceReadersMap;

        public Mapper()
        {
            proDeviceEnumerator = new SwitchProEnumerator();
            deviceReadersMap = new Dictionary<SwitchProDevice, SwitchProReader>();
        }

        public void Start()
        {
            // Change thread affinity of bus object to not be tied
            // to GUI thread
            vbusThr = new Thread(() =>
            {
                vigemTestClient = new ViGEmClient();
            });

            vbusThr.Priority = ThreadPriority.Normal;
            vbusThr.IsBackground = true;
            vbusThr.Start();
            vbusThr.Join(); // Wait for bus object start

            contThr = new Thread(() =>
            {
                outputX360 = vigemTestClient.CreateXbox360Controller();
                outputX360.AutoSubmitReport = false;
                outputX360.Connect();
            });
            contThr.Priority = ThreadPriority.Normal;
            contThr.IsBackground = true;
            contThr.Start();
            contThr.Join(); // Wait for bus object start

            Thread temper = new Thread(() =>
            {
                proDeviceEnumerator.FindControllers();
            });
            temper.IsBackground = true;
            temper.Priority = ThreadPriority.Normal;
            temper.Name = "HID Device Opener";
            temper.Start();
            temper.Join();

            // Populate with 0 first poll
            //filterX.Filter(0.0, 1.0 / 0.016);
            //filterY.Filter(0.0, 1.0 / 0.016);

            foreach (SwitchProDevice device in proDeviceEnumerator.GetProDevices())
            {
                //device.SetOperational();
                SwitchProReader proReader = new SwitchProReader(device);
                deviceReadersMap.Add(device, proReader);
                //proReader.Report += ProReader_Report;
                proReader.Report += Reader_Calibrate_Gyro;
                SwitchProDevice tmpDev = device;
                outputX360.FeedbackReceived += (sender, e) =>
                {
                    tmpDev.currentLeftAmpRatio = e.LargeMotor / 255.0;
                    tmpDev.currentRightAmpRatio = e.SmallMotor / 255.0;
                    proReader.WriteReport();
                };
                proReader.StartUpdate();
            }
        }

        /// <summary>
        /// Method used to hook the desired static mapping routine after gyro calibration is
        /// finished. Needed for now until a more dynamic solution is implemented.
        /// </summary>
        /// <param name="reader"></param>
        /// <param name="device"></param>
        private void HookReaderEvent(SwitchProReader reader, SwitchProDevice device)
        {
            reader.Report += ProReader_Report;
        }

        private void ProReader_Report(SwitchProReader sender, SwitchProDevice device)
        {
            ref SwitchProState current = ref device.ClothOff;
            ref SwitchProState previous = ref device.ClothOff2;
            outputX360.ResetReport();
            currentLatency = sender.CombLatency;
            currentRate = 1.0 / currentLatency;
            bool disableGyro = false;
            unchecked
            {
                ushort tempButtons = 0;
                if (current.A) tempButtons |= Xbox360Button.B.Value;
                if (current.B) tempButtons |= Xbox360Button.A.Value;
                if (current.X) tempButtons |= Xbox360Button.Y.Value;
                if (current.Y) tempButtons |= Xbox360Button.X.Value;
                if (current.Minus) tempButtons |= Xbox360Button.Back.Value;
                if (current.Plus) tempButtons |= Xbox360Button.Start.Value;
                //if (current.LShoulder) tempButtons |= Xbox360Button.LeftShoulder.Value;
                disableGyro = current.LShoulder;
                // Alt button to map LB when using Gyro controls. Will typically use L to disable Gyro
                if (current.Capture) tempButtons |= Xbox360Button.LeftShoulder.Value;
                if (current.RShoulder) tempButtons |= Xbox360Button.RightShoulder.Value;
                if (current.LSClick) tempButtons |= Xbox360Button.LeftThumb.Value;
                if (current.RSClick) tempButtons |= Xbox360Button.RightThumb.Value;
                if (current.Home) tempButtons |= Xbox360Button.Guide.Value;

                if (current.DpadUp) tempButtons |= Xbox360Button.Up.Value;
                if (current.DpadDown) tempButtons |= Xbox360Button.Down.Value;
                if (current.DpadLeft) tempButtons |= Xbox360Button.Left.Value;
                if (current.DpadRight) tempButtons |= Xbox360Button.Right.Value;

                outputX360.SetButtonsFull(tempButtons);
            }

            short temp;
            temp = AxisScale(current.LX, false);
            outputX360.LeftThumbX = temp;

            temp = AxisScale(current.LY, false);
            outputX360.LeftThumbY = temp;

            //temp = AxisScale(current.RX, false);
            //outputX360.RightThumbX = temp;

            //temp = AxisScale(current.RY, false);
            //outputX360.RightThumbY = temp;

            // Skip if duration is less than 10 ms
            if (current.timeElapsed > 0.01)
            {
                if (!disableGyro)
                {
                    PrepareGyroMouseJoystickEvent(ref current, ref previous);
                }
                else
                {
                    // Take possible lag state into account. Main routine will make sure to skip this method
                    //if (previous.timeElapsed <= 0.002)
                    //{
                    //    double timeElapsed = current.timeElapsed;
                    //    timeElapsed += previous.timeElapsed;
                    //    currentRate = 1.0 / timeElapsed;
                    //}

                    filterX.Filter(0.0, currentRate); // Smooth on output
                    filterY.Filter(0.0, currentRate); // Smooth on output

                    //outputX360.RightThumbX = 0;
                    //outputX360.RightThumbY = 0;
                }

                sender.CombLatency = 0;
            }

            outputX360.LeftTrigger = current.ZL ? (byte)255 : (byte)0;
            outputX360.RightTrigger = current.ZR ? (byte)255 : (byte)0;

            /*if (current.ZL != previous.ZL)
            {
                mouseRBDown = current.ZL;
                InputMethods.MouseEvent(current.ZL ? InputMethods.MOUSEEVENTF_RIGHTDOWN :
                    InputMethods.MOUSEEVENTF_RIGHTUP);
            }

            if (current.ZR != previous.ZR)
            {
                mouseLBDown = current.ZR;
                InputMethods.MouseEvent(current.ZR ? InputMethods.MOUSEEVENTF_LEFTDOWN :
                    InputMethods.MOUSEEVENTF_LEFTUP);
            }*/

            // Skip if duration is less than 10 ms
            /*bool tempSkipMouse = false;
            if (current.timeElapsed > 0.01)
            {
                if (!disableGyro)
                {
                    PopulateGyroMouseEventData(ref current, ref previous);
                }
                else
                {
                    mouseXRemainder = mouseYRemainder = 0.0;
                    // Take possible lag state into account. Main routine will make sure to skip this method
                    //if (previous.timeElapsed <= 0.002)
                    //{
                    //    double timeElapsed = current.timeElapsed;
                    //    timeElapsed += previous.timeElapsed;
                    //    currentRate = 1.0 / timeElapsed;
                    //}
                }

                sender.CombLatency = 0;
            }
            else
            {
                tempSkipMouse = true;
            }

            if (!tempSkipMouse)
            {
                if (mouseX != 0.0 || mouseY != 0.0)
                {
                    //Console.WriteLine("MOVE: {0}, {1}", (int)mouseX, (int)mouseY);
                    GenerateMouseMoveEvent();
                }
                else
                {
                    // Probably not needed here. Leave as a temporary precaution
                    mouseXRemainder = mouseYRemainder = 0.0;

                    filterX.Filter(0.0, currentRate); // Smooth on output
                    filterY.Filter(0.0, currentRate); // Smooth on output
                }
            }
            */

            outputX360.SubmitReport();
            //sender.CombLatency = 0;
            //Console.WriteLine("CURRENT B: {0}", current.B);
        }

        //private bool rightGyroCalibrated = false;
        private const int CALIB_POLL_COUNT = 240; // Roughly 4 seconds of polls
        private int rightGyroCalibPolls = 0;
        private int ignoreGyroCalibPolls = 0;
        private const int TOTAL_IGNORE_CALIB_POLL_COUNT = 67; // Roughly 1 second of polls
        private int gyroYawCalibSum = 0;
        private int gyroPitchCalibSum = 0;
        private int gyroRollCalibSum = 0;

        private void Reader_Calibrate_Gyro(SwitchProReader sender, SwitchProDevice device)
        {
            ref SwitchProState current = ref device.ClothOff;

            if (ignoreGyroCalibPolls < TOTAL_IGNORE_CALIB_POLL_COUNT)
            {
                ignoreGyroCalibPolls++;
                return;
            }

            if (rightGyroCalibPolls == 0)
            {
                Console.WriteLine("Starting Calib");
            }

            gyroYawCalibSum += current.Motion.GyroYaw;
            gyroPitchCalibSum += current.Motion.GyroPitch;
            gyroRollCalibSum += current.Motion.GyroRoll;

            rightGyroCalibPolls++;
            if (rightGyroCalibPolls >= CALIB_POLL_COUNT)
            {
                //rightGyroCalibrated = true;

                /*Console.WriteLine("OBTAINED DESIRED POLL COUNT. CALCULATE OFFSETS.");
                Console.WriteLine("OBTAINED DESIRED POLL COUNT. CALCULATE OFFSETS.");
                Console.WriteLine("OBTAINED DESIRED POLL COUNT. CALCULATE OFFSETS.");
                Console.WriteLine("OBTAINED DESIRED POLL COUNT. CALCULATE OFFSETS.");
                Console.WriteLine("OBTAINED DESIRED POLL COUNT. CALCULATE OFFSETS.");

                Console.WriteLine("OBTAINED DESIRED POLL COUNT. CALCULATE OFFSETS.");
                Console.WriteLine("OBTAINED DESIRED POLL COUNT. CALCULATE OFFSETS.");
                Console.WriteLine("OBTAINED DESIRED POLL COUNT. CALCULATE OFFSETS.");
                Console.WriteLine("OBTAINED DESIRED POLL COUNT. CALCULATE OFFSETS.");
                Console.WriteLine("OBTAINED DESIRED POLL COUNT. CALCULATE OFFSETS.");

                Console.WriteLine("OBTAINED DESIRED POLL COUNT. CALCULATE OFFSETS.");
                Console.WriteLine("OBTAINED DESIRED POLL COUNT. CALCULATE OFFSETS.");
                Console.WriteLine("OBTAINED DESIRED POLL COUNT. CALCULATE OFFSETS.");
                Console.WriteLine("OBTAINED DESIRED POLL COUNT. CALCULATE OFFSETS.");
                Console.WriteLine("OBTAINED DESIRED POLL COUNT. CALCULATE OFFSETS.");*/

                device.gyroCalibOffsets[SwitchProDevice.IMU_YAW_IDX] = (short)(gyroYawCalibSum / rightGyroCalibPolls);
                device.gyroCalibOffsets[SwitchProDevice.IMU_PITCH_IDX] = (short)(gyroPitchCalibSum / rightGyroCalibPolls);
                device.gyroCalibOffsets[SwitchProDevice.IMU_ROLL_IDX] = (short)(gyroRollCalibSum / rightGyroCalibPolls);
                Console.WriteLine(string.Join(",", device.gyroCalibOffsets));

                sender.Report -= Reader_Calibrate_Gyro;
                HookReaderEvent(sender, device);
                //sender.Report += Reader_Right_Mixed_Gyro_Report;
                Console.WriteLine("CALIB DONE");
            }
        }

        private void PopulateGyroMouseEventData(ref SwitchProState current, ref SwitchProState previous)
        {
            //const int deadZone = 28;
            const int deadZone = 18;
            const double GYRO_MOUSE_COEFFICIENT = 0.0095;
            const double GYRO_MOUSE_OFFSET = 0.85;
            //const double GYRO_MOUSE_OFFSET = 0.0;

            double offset = GYRO_MOUSE_OFFSET;
            double coefficient = GYRO_MOUSE_COEFFICIENT;

            double timeElapsed = currentLatency;
            //double timeElapsed = current.timeElapsed;
            // Take possible lag state into account. Main routine will make sure to skip this method
            //if (previous.timeElapsed <= 0.002)
            //{
            //    timeElapsed += previous.timeElapsed;
            //    currentRate = 1.0 / timeElapsed;
            //}

            // Base speed 5 ms
            double tempDouble = timeElapsed * 3 * 66.67;
            int deltaX = current.Motion.GyroYaw, deltaY = current.Motion.GyroPitch;
            double tempAngle = Math.Atan2(-deltaY, deltaX);
            double normX = Math.Abs(Math.Cos(tempAngle));
            double normY = Math.Abs(Math.Sin(tempAngle));
            int signX = Math.Sign(deltaX);
            int signY = Math.Sign(deltaY);

            int deadzoneX = (int)Math.Abs(normX * deadZone);
            int deadzoneY = (int)Math.Abs(normY * deadZone);

            if (Math.Abs(deltaX) > deadzoneX)
            {
                deltaX -= signX * deadzoneX;
            }
            else
            {
                deltaX = 0;
            }

            if (Math.Abs(deltaY) > deadzoneY)
            {
                deltaY -= signY * deadzoneY;
            }
            else
            {
                deltaY = 0;
            }

            double xMotion = deltaX != 0 ? coefficient * (deltaX * tempDouble)
                + (normX * (offset * signX)) : 0;

            double yMotion = deltaY != 0 ? coefficient * (deltaY * tempDouble)
                + (normY * (offset * signY)) : 0;

            mouseX = xMotion; mouseY = yMotion;
        }

        private void PrepareGyroMouseJoystickEvent(ref SwitchProState current, ref SwitchProState previous)
        {
            //const int deadzone = 24;
            const int deadzone = 24;
            const int maxZone = 600;
            //const double antidead = 0.54;
            const double antidead = 0.45;

            double timeElapsed = currentLatency;
            //double timeElapsed = current.timeElapsed;
            //// Take possible lag state into account. Main routine will make sure to skip this method
            //if (previous.timeElapsed <= 0.002)
            //{
            //    timeElapsed += previous.timeElapsed;
            //    currentRate = 1.0 / timeElapsed;
            //}

            // Base speed 15 ms
            double tempDouble = timeElapsed * 66.67;
            //Console.WriteLine("Elasped: ({0}) DOUBLE {1}", current.timeElapsed, tempDouble);
            int deltaX = current.Motion.GyroYaw, deltaY = -current.Motion.GyroPitch;
            int maxDirX = deltaX >= 0 ? X360_STICK_MAX : X360_STICK_MIN;
            int maxDirY = deltaY >= 0 ? X360_STICK_MAX : X360_STICK_MIN;
            //int maxDirX = deltaX >= 0 ? 127 : -128;
            //int maxDirY = deltaY >= 0 ? 127 : -128;
            double tempAngle = Math.Atan2(-deltaY, deltaX);
            double normX = Math.Abs(Math.Cos(tempAngle));
            double normY = Math.Abs(Math.Sin(tempAngle));
            int signX = Math.Sign(deltaX);
            int signY = Math.Sign(deltaY);

            int deadzoneX = (int)Math.Abs(normX * deadzone);
            int deadzoneY = (int)Math.Abs(normY * deadzone);

            int maxValX = signX * maxZone;
            int maxValY = signY * maxZone;

            double xratio = 0.0, yratio = 0.0;
            double antiX = antidead * normX;
            double antiY = antidead * normY;

            if (Math.Abs(deltaX) > deadzoneX)
            {
                deltaX -= signX * deadzoneX;
                deltaX = (int)(deltaX * tempDouble);
                deltaX = (deltaX < 0 && deltaX < maxValX) ? maxValX :
                    (deltaX > 0 && deltaX > maxValX) ? maxValX : deltaX;
                //if (deltaX != maxValX) deltaX -= deltaX % (signX * GyroMouseFuzz);
            }
            else
            {
                deltaX = 0;
            }

            if (Math.Abs(deltaY) > deadzoneY)
            {
                deltaY -= signY * deadzoneY;
                deltaY = (int)(deltaY * tempDouble);
                deltaY = (deltaY < 0 && deltaY < maxValY) ? maxValY :
                    (deltaY > 0 && deltaY > maxValY) ? maxValY : deltaY;
                //if (deltaY != maxValY) deltaY -= deltaY % (signY * GyroMouseFuzz);
            }
            else
            {
                deltaY = 0;
            }

            if (deltaX != 0) xratio = deltaX / (double)maxValX;
            if (deltaY != 0) yratio = deltaY / (double)maxValY;

            double xNorm = 0.0, yNorm = 0.0;
            if (xratio != 0.0)
            {
                xNorm = (1.0 - antiX) * xratio + antiX;
            }

            if (yratio != 0.0)
            {
                yNorm = (1.0 - antiY) * yratio + antiY;
            }

            //byte axisXOut = (byte)(xNorm * maxDirX + 128.0);
            //byte axisYOut = (byte)(yNorm * maxDirY + 128.0);
            bool useGyro = false;
            /*if (xNorm != 0.0 || yNorm != 0.0)
            {
                double tempX = (current.RX - STICK_MIN) * reciprocalInputResolution;
                double tempY = (current.RY - STICK_MIN) * reciprocalInputResolution;

                if (xNorm > tempX || yNorm > tempY)
                {
                    useGyro = true;
                }
            }
            */

            useGyro = true;
            if (useGyro)
            {
                double RX = xNorm * maxDirX + 0.0;
                double RY = yNorm * maxDirY + 0.0;
                outputX360.RightThumbX = (short)filterX.Filter(RX, currentRate); // Smooth on output
                outputX360.RightThumbY = (short)filterY.Filter(RY, currentRate); // Smooth on output
                //outputX360.RightThumbX = (short)(xNorm * maxDirX + 0.0);
                //outputX360.RightThumbY = (short)(yNorm * maxDirY + 0.0);
                //outputX360.RightThumbX = (short)(xNorm * maxDirX + 0.0);
                //outputX360.RightThumbY = (short)(yNorm * maxDirY + 0.0);
            }
            else
            {
                filterX.Filter(0.0, currentRate); // Smooth on output
                filterY.Filter(0.0, currentRate); // Smooth on output

                outputX360.RightThumbX = 0;
                outputX360.RightThumbY = 0;
            }
        }

        private void PopulateMouseEventData(ref SwitchProState current)
        {
            const double deadZone = 0.10; //0.08;
            double angle = Math.Atan2(current.RY - STICK_NEUTRAL, current.RX - STICK_NEUTRAL);
            double angCos = Math.Abs(Math.Cos(angle)),
                angSin = Math.Abs(Math.Sin(angle));

            int currentX = current.RX - STICK_NEUTRAL;
            int maxX = currentX >= 0.0 ? STICK_MAX : STICK_MIN;
            int maxDirX = currentX >= 0.0 ? STICK_MIDRANGE : -STICK_MIDRANGE;

            int currentY = current.RY - STICK_NEUTRAL;
            int maxY = currentY >= 0.0 ? STICK_MAX : STICK_MIN;
            int maxDirY = currentY >= 0.0 ? STICK_MIDRANGE : -STICK_MIDRANGE;

            int currentDeadX = (int)(deadZone * maxDirX * angCos);
            int currentDeadY = (int)(deadZone * maxDirY * angSin);

            double stickDeadzoneSquared = (currentDeadX * currentDeadX) + (currentDeadY * currentDeadY);
            double stickSquared = Math.Pow(currentX, 2) + Math.Pow(currentY, 2);
            bool inSafeZone = stickSquared > stickDeadzoneSquared;

            if (inSafeZone)
            {
                double timeDelta = current.timeElapsed * 0.001;
                int mouseVelocity = mouseSpeed * MOUSESPEEDFACTOR;
                double mouseOffset = MOUSE_VELOCITY_OFFSET * mouseVelocity;

                double xNorm = (currentX - currentDeadX) / (double)(maxDirX - currentDeadX);
                double yNorm = (currentY - currentDeadY) / (double)(maxDirY - currentDeadY);

                double xSign = currentX >= 0.0 ? 1.0 : -1.0;
                double ySign = currentY >= 0.0 ? 1.0 : -1.0;
                double xUnit = Math.Abs(xNorm);
                double yUnit = Math.Abs(yNorm);
                double tempMouseOffsetX = xUnit * mouseOffset;
                double tempMouseOffsetY = yUnit * mouseOffset;

                mouseX = ((mouseVelocity - tempMouseOffsetX) * timeDelta * xUnit + (tempMouseOffsetX * timeDelta)) * xSign;
                mouseY = ((mouseVelocity - tempMouseOffsetY) * timeDelta * yUnit + (tempMouseOffsetY * timeDelta)) * -ySign;
                //Console.WriteLine("X({0}) ({1}) Y({2}) ({3}) ({4})", mouseX, xNorm * xSign, mouseY, yNorm * ySign, timeDelta);
            }
            else
            {
                //Console.WriteLine("IN DEAD ZONE:");
            }
        }

        private short AxisScale(int value, bool flip)
        {
            unchecked
            {
                float temp = (value - STICK_MIN) * reciprocalInputResolution;
                if (flip) temp = (temp - 0.5f) * -1.0f + 0.5f;
                return (short)(temp * OUTPUT_X360_RESOLUTION + X360_STICK_MIN);
            }
        }

        private void GenerateMouseMoveEvent()
        {
            if (mouseX != 0.0 || mouseY != 0.0)
            {
                if ((mouseX > 0.0 && mouseXRemainder > 0.0) || (mouseX < 0.0 && mouseXRemainder < 0.0))
                {
                    mouseX += mouseXRemainder;
                }
                else
                {
                    mouseXRemainder = 0.0;
                }

                if ((mouseY > 0.0 && mouseYRemainder > 0.0) || (mouseY < 0.0 && mouseYRemainder < 0.0))
                {
                    mouseY += mouseYRemainder;
                }
                else
                {
                    mouseYRemainder = 0.0;
                }

                //mouseX = filterX.Filter(mouseX, 1.0 / 0.016);
                //mouseY = filterY.Filter(mouseY, 1.0 / 0.016);
                mouseX = filterX.Filter(mouseX, currentRate);
                mouseY = filterY.Filter(mouseY, currentRate);

                double mouseXTemp = mouseX - (remainderCutoff(mouseX * 1000.0, 1.0) / 1000.0);
                int mouseXInt = (int)(mouseXTemp);
                mouseXRemainder = mouseXTemp - mouseXInt;

                double mouseYTemp = mouseY - (remainderCutoff(mouseY * 1000.0, 1.0) / 1000.0);
                int mouseYInt = (int)(mouseYTemp);
                mouseYRemainder = mouseYTemp - mouseYInt;
                InputMethods.MoveCursorBy(mouseXInt, mouseYInt);
            }
            else
            {
                mouseXRemainder = mouseYRemainder = 0.0;
                //mouseX = filterX.Filter(0.0, 1.0 / 0.016);
                //mouseY = filterY.Filter(0.0, 1.0 / 0.016);
                filterX.Filter(mouseX, currentRate);
                filterY.Filter(mouseY, currentRate);
            }

            mouseX = mouseY = 0.0;
        }

        private double remainderCutoff(double dividend, double divisor)
        {
            return dividend - (divisor * (int)(dividend / divisor));
        }

        public void Stop()
        {
            foreach(SwitchProReader readers in deviceReadersMap.Values)
            {
                //readers.StopUpdate();
            }

            deviceReadersMap.Clear();
            proDeviceEnumerator.StopControllers();

            outputX360?.Disconnect();
            outputX360 = null;

            vigemTestClient?.Dispose();
            vigemTestClient = null;
        }
    }
}
