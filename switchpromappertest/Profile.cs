﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace switchpromappertest
{
    public class Profile
    {
        private List<ActionSet> actionSets = new List<ActionSet>(8);

        private ActionSet currentSet;
        public ActionSet CurrentSet { get => currentSet; }

        private string name;
        public string Name { get => name; set => name = value; }

        private string description;
        public string Description { get => description; set => description = value; }

        public Profile()
        {
            ActionSet actionSet = new ActionSet();
            actionSets.Add(actionSet);
        }
    }
}
