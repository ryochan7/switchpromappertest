﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace switchpromappertest
{
    public class ActionSet
    {
        private List<ActionLayer> actionLayers = new List<ActionLayer>(4);
        private ActionLayer currentActionLayer;
        public ActionLayer CurrentActionLayer { get => currentActionLayer; }


        /// <summary>
        /// Will contain all mapped actions associated in the sets
        /// for all ActionLayer instances
        /// </summary>
        private List<MapAction> setActions = new List<MapAction>();

        private string name;
        public string Name { get => name; set => name = value; }

        private string description;
        public string Description { get => description; set => description = value; }

        public ActionSet()
        {
            ActionLayer actionLayer = new ActionLayer();
            actionLayers.Add(actionLayer);

            currentActionLayer = actionLayer;
        }
    }
}
