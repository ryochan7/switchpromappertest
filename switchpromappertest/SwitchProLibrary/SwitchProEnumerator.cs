﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HidLibrary;

namespace switchpromappertest.SwitchProLibrary
{
    public class SwitchProEnumerator
    {
        private const int NINTENDO_VENDOR_ID = 0x57e;
        private const int SWITCH_PRO_PRODUCT_ID = 0x2009;

        private Dictionary<string, SwitchProDevice> foundDevices;

        public SwitchProEnumerator()
        {
            foundDevices = new Dictionary<string, SwitchProDevice>();
        }

        public void FindControllers()
        {
            IEnumerable<HidDevice> hDevices = HidDevices.Enumerate(NINTENDO_VENDOR_ID,
                SWITCH_PRO_PRODUCT_ID);
            List<HidDevice> tempList = hDevices.ToList();
            //int devCount = tempList.Count();
            //string devicePlural = "device" + (devCount == 0 || devCount > 1 ? "s" : "");

            foreach(HidDevice hDevice in tempList)
            {
                if (!hDevice.IsOpen)
                {
                    hDevice.OpenDevice(false);
                }

                if (hDevice.IsOpen)
                {
                    //string serial = hDevice.readSerial();
                    SwitchProDevice tempDev = new SwitchProDevice(hDevice);
                    foundDevices.Add(hDevice.DevicePath, tempDev);
                }
            }
        }

        public IEnumerable<SwitchProDevice> GetProDevices()
        {
            return foundDevices.Values.ToList();
        }

        public void RemoveDevice(SwitchProDevice proDevice)
        {
            proDevice.Detach();
            proDevice.HidDevice.CloseDevice();
            foundDevices.Remove(proDevice.HidDevice.DevicePath);
        }

        public void StopControllers()
        {
            foreach(SwitchProDevice proDevice in foundDevices.Values)
            {
                proDevice.Detach();
                proDevice.HidDevice.CloseDevice();
            }

            foundDevices.Clear();
        }
    }
}
