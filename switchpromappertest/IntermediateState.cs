﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace switchpromappertest
{
    /// <summary>
    /// Mapping state used between raw input device state and final output device state
    /// </summary>
    class IntermediateState
    {
        public enum Buttons : uint
        {
            None = 0,
            Button1 = 1,
            A = Button1,
            Button2 = 2,
            B = Button2,
            Button3 = 3,
            X = Button3,
            Button4 = 4,
            Y = Button4,
        }

        public enum Axis : uint
        {
            None = 0,
            Axis1 = 1,
            LX = Axis1,
            Axis2 = 2,
            LY = Axis2,
            Axis3 = 3,
            RX = Axis3,
            Axis4 = 4,
            RY = Axis4,
        }

        public uint buttons;
        public short leftStickX;
        public short leftStickY;
        public short rightStickX;
        public short rightStickY;
        public ushort leftTrigger;
        public ushort rightTrigger;
    }
}
